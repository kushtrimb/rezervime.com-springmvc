package com.rezervimi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rezervimi.entity.User;
import com.rezervimi.repository.TestRepository;
import com.rezervimi.repository.UserRepositoryE;
import com.rezervimi.service.UserService;

@RestController
@RequestMapping("/api")
public class TestController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserRepositoryE userRep;
	@Autowired
	private TestRepository testRepository;
	
	@RequestMapping(value="/test", method=RequestMethod.GET,headers = "Accept=application/json")
	public List<User> addAbonim(){
		return testRepository.findAll();
	   // return userService.getAllUsers();
	}
	
	
	@RequestMapping(value = "/datatables-view", method = RequestMethod.POST)
	public DataTablesOutput<User> getUsersForDatatables(@Valid @RequestBody DataTablesInput input) {
	
		return null;
	}
	

}
