package com.rezervimi.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.rezervimi.entity.User;

public interface TestRepository extends Repository<User, Long> {

	  List<User> findAll();
	}