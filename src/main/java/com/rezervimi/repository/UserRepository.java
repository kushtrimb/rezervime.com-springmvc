package com.rezervimi.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rezervimi.entity.User;


@Repository
@Transactional
public class UserRepository{

	@Autowired
	private SessionFactory session;
	
	public List<User> findAll(){
		return session.getCurrentSession().getNamedQuery("User.findAll").list();
	} 
	
	
	
}