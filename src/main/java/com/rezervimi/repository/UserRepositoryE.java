package com.rezervimi.repository;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import com.rezervimi.entity.User;

public interface UserRepositoryE extends DataTablesRepository<User, Integer>{

}
