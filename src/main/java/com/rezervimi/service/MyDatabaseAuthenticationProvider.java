package com.rezervimi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

public class MyDatabaseAuthenticationProvider implements AuthenticationProvider {

	/*@Autowired
	private UserRepository userrep;
	@Autowired
	private BCrypt encrypt;
	@Autowired
	private LoginAttempts loginAttempts;*/

	@Override
	public Authentication authenticate(final Authentication authentication) {
		/*String username = (String) authentication.getPrincipal();
		String password = encrypt.encryptPassword((String) authentication.getCredentials());
		
		boolean fbaccess = false;
		
		if(username.startsWith("bubrreci") && username.endsWith("milingona")){
			fbaccess = true;
			username = username.split(";")[1];
			password = (String)authentication.getCredentials();
		}

		Authentication auth = null;
		User user = userrep.getUserByCred(username, password);
		System.out.print(user);
		if (user != null && (user.getFb() == null || fbaccess)) {
			if (user.getStatus() == 0) {
				throw new DisabledException("Disabled");
			}
			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			List<UserRole> roles = user.getUserRoles();
			for (UserRole r : roles) {
				System.out.println(r.getRole().getName());
				grantedAuths.add(new SimpleGrantedAuthority(r.getRole().getName()));
			}
			auth = new UsernamePasswordAuthenticationToken(user.getUsername(), null, grantedAuths);
			user.setLastLogin(new Date());
			user.setOnline((byte)1);
			userrep.editUser(user);
			return auth;
		} else {
			if (loginAttempts.getUsername().equals(username)) {
				if (loginAttempts.getAttempts() < 5) {
					loginAttempts.incrementAttempts();
					System.out.print("===>"+loginAttempts.getAttempts());
				} else {
					User userSt = userrep.getUserByUsername(username);
					userSt.setStatus((byte) 0);
					userrep.editUser(userSt);
					throw new DisabledException("Disabled");
				}
			} else {
				loginAttempts.setUsername(username);
				loginAttempts.setAttempts(0);
				System.out.print("restet");
			}
			throw new BadCredentialsException("Bad Credentials");
		}*/
       return null;
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
