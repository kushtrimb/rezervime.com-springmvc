angular.module('templates-app', ['account/login.tpl.html', 'account/register.tpl.html', 'cms/dashboard.tpl.html', 'home/home.tpl.html', 'home/personal.tpl.html']);

angular.module("account/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/login.tpl.html",
    "<h1>Logintpl</h1>");
}]);

angular.module("account/register.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("account/register.tpl.html",
    "<h1>Registertpl</h1>");
}]);

angular.module("cms/dashboard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("cms/dashboard.tpl.html",
    "<h1>dashboard</h1>");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<h1>Hommetpl</h1>");
}]);

angular.module("home/personal.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/personal.tpl.html",
    "<h1>personaltpl</h1>");
}]);
