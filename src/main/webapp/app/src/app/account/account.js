angular.module('ngBoilerplate.account', ['ui.router', 'ngResource', 'base64', 'ngCookies', 'ngRoute','datatables'])
.config(function($stateProvider) {
    $stateProvider.state('manageUser', {
        url:'/manage/user?accountId',
        views: {
            'main': {
                templateUrl:'cms/manage-user.tpl.html',
                controller: 'ManageUserCtrl'
            }
        },
        resolve: {
            account: function(accountService, $stateParams) {
                return accountService.getAccountById($stateParams.accountId);
            },
            galeries: function(accountService, $stateParams) {
                return accountService.getGalery($stateParams.accountId);
            }
        },
        data : { pageTitle : "CMS" }
});
})
.factory('sessionService', function($http, $base64, $cookies,$resource) {
    var session = {};
    session.login = function(data) {
        return $http.post("/TuDate/oauth/token", "grant_type=" + "password" + "&username=" + data.name +
                "&password=" + data.password + "&client_id=restapp" + "&client_secret=restapp", {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        } ).then(function(data) {
           $cookies.put("token", data.data.value);
        }, function(data) {
            alert("username or password are incorrect");
        });
    };
    session.logout = function() {       
        var Account = $resource("/TuDate/api/lastLogin/?access_token="+$cookies.get("token"));
        Account.save({});
        delete $cookies["token"];
        delete $cookies["account"];
    };
    session.isLoggedIn = function() {
        return $cookies.get("token") !== undefined;
    };
    return session;
})
.factory('accountInSessionService', function($resource,$cookies, $q, $timeout, $http) {
    return {
          myMethod: function() {
               // return the same promise that $http.get returns
               return $http.get('/TuDate/api/userInSession/?access_token='+$cookies.get("token"));
              }
           };
})
.factory('accountService', function($resource,$cookies, $q, $timeout, $http) {
    var service = {};
    service.register = function(account, success, failure) {
        var Account = $resource("/TuDate/api/registerUser");
        Account.save({}, account, success, failure);
    };
    service.getAccountById = function(accountId) {
        var Account = $resource("/TuDate/api/getUser/:paramAccountId/?access_token="+$cookies.get("token"));
        return Account.get({paramAccountId:accountId}).$promise;
    };       
    service.userExists = function(account, success, failure) {
        var Account = $resource("/TuDate/api/isUsernameFree/"+account.username);
        var data = Account.get({}, function() {
            if(data[0] === "1"){
               success();
            }else{
               failure();
            }
        },
        failure);
    };
    service.getAllAccounts = function() {
          var Account = $resource("/TuDate/api/users/?access_token="+$cookies.get("token"));
          return Account.query().$promise.then(function(data) {
             console.log(data);
            return data;
          });
      };
    return service;
})
.controller("LoginCtrl", function($scope, sessionService, accountService, $state, $window,$cookies,accountInSessionService, langTranslationsService) {
    $scope.login = function() {
            sessionService.login($scope.account).then(function() {
                  langTranslationsService.translations(30).then(function(result) {
                    $cookies.put("translations",JSON.stringify(result.data));
                  }).then(function(result) {
                  });  
                  accountInSessionService.myMethod().then(function(result) {
                    $cookies.put("account",JSON.stringify(result.data));                 
                    if(result.data.role === "ROLE_ADMIN"){
                      $window.location.href = '/TuDate/app/cms.html#/cms/dashboard';
                    }else{
                      $window.location.href = '/TuDate/app/index.html#/home'; 
                    }
                  }).then(function(result) {
                  });
                                  
            });
            
    };    
    $scope.logout = function(){
        sessionService.logout();
        $window.location.href = '/TuDate/app/login.html';       
    };
})
.controller("RegisterCtrl", function($scope, sessionService, $state, accountService) {
    $scope.register = function() {
        var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
        if($scope.account === undefined || $scope.account.name === undefined || $scope.account.name  === '' || $scope.account.surname === undefined || $scope.account.surname  === '' || $scope.account.username === undefined || $scope.account.username  === '' || $scope.account.password === undefined || $scope.account.password  === '' || $scope.account.email === undefined || $scope.account.email  === ''){
          alert("mbush fushat e kerkuara!!");
        }
        else if($scope.account.password.length < 8){
           alert("password must have at least 8 chars");
        }
        else if($scope.account.email  === '' || !re.test($scope.account.email)){
           alert("not valid email address!");
        }
        else{
           accountService.userExists($scope.account,function(){
                 alert("Username is taken!!");
              },function(){
                accountService.register($scope.account,
                  function(returnedData) {
                     $scope.account = null;
                   },
                  function() {
                     alert("Error registering user");
                });
           });
        }
    };
})
.controller("KyquCtrl", function($scope, $window){
	$scope.kyqu = function(){
		$window.location.href = '/TuDate/app/login.html';
	};
})
.controller("ManageUserCtrl", function($scope, account, $state, galeries, profilePicService) {
    $scope.account = account;
});