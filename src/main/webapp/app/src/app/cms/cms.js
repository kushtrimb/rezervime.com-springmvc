angular.module('ngBoilerplate.cms', ['ui.router', 'ngResource', 'base64', 'ngCookies', 'ngRoute', 'datatables'])
.config(function($stateProvider) {
    $stateProvider.state('dashboard', {
    url:'/cms/dashboard',
    views: {
        'main': {
            templateUrl:'cms/dashboard.tpl.html',
            controller: 'CMSDashboardCtrl'
        }       
    },
    data : { pageTitle : "CMS" },
    resolve: {
        statistics: function(manageService, $cookies, $window) {
             if($cookies.get("token") === undefined){
                $window.location.href = '/TuDate/app/login.html'; 
             }
           return manageService.getStatistics();
        }
    }
});
})
.controller("CMSDashboardCtrl", function($scope, statistics) {
     $scope.statistics = statistics;
});